require 'test_helper'

class BtcControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get btc_index_url
    assert_response :success
  end

  test "should get show" do
    get btc_show_url
    assert_response :success
  end

end
