class CreateBtcs < ActiveRecord::Migration[5.2]
  def change
    create_table :btcs do |t|
      t.string :uid
      t.string :address
      t.integer :amount
      t.string :request

      t.timestamps
    end
  end
end
