class TopController < ApplicationController
  def main
		if session[:login_uid]
      $id = User.find_by(uid: session[:login_uid]).id
			redirect_to main_index_path
		else
			redirect_to top_login_path
		end
	end

	def login
		user = User.authenticate(params[:uid], params[:pass])
		if user
			session[:login_uid] = params[:uid]
			redirect_to top_main_path
    elsif params[:uid] == nil && params[:pass] == nil
		else
      flash.now[:er] = "ログインに失敗しました"
			render top_login_path
		end
	end

	def logout
		session.delete(:login_uid)
		redirect_to top_login_path
	end
end
