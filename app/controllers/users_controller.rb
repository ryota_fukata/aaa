class UsersController < ApplicationController

  def index
    @users = User.all
  end

  def new
    @users = User.new
  end

  def create
    @users = User.new(
      uid: params[:user][:uid],
      password: params[:user][:password],
      password_confirmation: params[:user][:password_confirmation],
      exe: 0)
    if @users.save
      redirect_to top_login_path
    else
      render 'new'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    redirect_to users_path
  end

  def show
    @user = current_user
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    @user.update(
       uid: params[:user][:uid],
       password: params[:user][:password],
       password_confirmation: params[:user][:password_confirmation],
       exe: @user.exe)
    session[:login_uid] = params[:user][:uid]
    render users_show_path
  end
end
