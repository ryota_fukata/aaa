class MainController < ApplicationController
  def index
    @user = current_user
    @userall = User.all
    @u = @userall.sort_by(&:exe).reverse
  end

  def point
    @user = current_user
  end

  def sen
    @user = current_user
    if @user.exe >= 100
      @user.exe -= 100
      @user.save
      flash.now[:not] = "送金しました"
    else
      flash.now[:not] = "ポイント不足のため送金できませんでした"
    end
  end
end
