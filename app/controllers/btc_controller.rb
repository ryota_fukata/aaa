class BtcController < ApplicationController
  def index
    @btc = Btc.all
    redirect_to btc_new_path
  end

  def new
    @btc = Btc.new
  end

  def create
    @btc = Btc.new(
      uid: current_user,
      address: params[:btc][:address],
      amount: 0.001,
      request: 送金準備中)
    if @btc.save
      redirect_to main_index_path
    else
      render 'new'
    end
  end

  def show
    @btc = Btc.all
  end
end
