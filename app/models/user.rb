class User < ApplicationRecord
  has_many :btc
  attr_accessor :password, :password_confirmation

def password=(val)
  if val.present?
    self.pass = BCrypt::Password.create(val)
  end
  @password = val
end

def self.authenticate(uid, pass)
  user = User.find_by_uid(uid)
  if user && BCrypt::Password.new(user.pass) == pass
    user
  else
    nil
  end
end
end
